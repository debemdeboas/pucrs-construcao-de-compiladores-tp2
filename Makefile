JFLEX  = java -jar jflex.jar
JAVAC  = javac

# targets:

all: JSONLexer.class

clean:
	rm -f *~ *.class Yylex.java

JSONLexer.class: JSONLexer.java Yylex.java
	$(JAVAC) JSONLexer.java

Yylex.java: json.flex
	$(JFLEX) json.flex
