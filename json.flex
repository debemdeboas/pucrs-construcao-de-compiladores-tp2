/*
jflex json.flex ; echo ; java JSONFlexer.java example.json
*/

import java.io.InputStreamReader;

%%

%public
%integer
%line
%char

%{
  private JSONLexer yyparser;

  public Yylex(java.io.Reader r, JSONLexer yyparser) {
    this(r);
    this.yyparser = yyparser;
  }

  public static final int NUM    = 302;
  public static final int STRING = 303;
  public static final int INVALID_TOKEN = 666;
%}

STRING = \"[^\"]+\"
DIGIT  = [0-9]+(\.[0-9]+)?
WHITESPACE = [ \t\n\r]

%%

{STRING}      {return STRING;}
{DIGIT}       {return NUM;}
{WHITESPACE}+ {}

"{" | "}" | ":" | "," | "[" | "]" {return yytext().charAt(0);}
.         {System.out.println(yyline+1 + ": caracter invalido: "+yytext());}
